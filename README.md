# kitabisa-screening-test
###### [Bayu Firmawan Paoh](https://www.linkedin.com/in/bayupaoh/) - Software Engineer iOS
Basic technical test for ios developer screening test at KitaBisa

### Overview
* SwiftUI for build UI
* MVVM for architecture
* CoreData for local database
* Unit Test (Not all are covered)
* Support dual language (🇺🇸, 🇮🇩)
* [Alamofire](https://github.com/Alamofire/Alamofire) - Elegent networking for swift
* [KingFisher](https://github.com/onevcat/Kingfisher) - Download and caching image.
* [SwiftLint](https://github.com/realm/SwiftLint) - Swift style and conventions
* [OHHTTPStubs/Swift](https://github.com/AliSoftware/OHHTTPStubs) - Test your apps with fake network data and custom response time, response code and headers

### Environment
XCode 11.3
Swift 5

### Demo
![demo](https://media.giphy.com/media/UQPTKWaOxzdzXAj3Ib/giphy.gif)
